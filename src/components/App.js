import React from "react";

import "./App.css";
import ContactList from "./ContactList";

const App = () => {
  return (
    <div className="container bg-success">
      <div className="row d-flex justify-content-center">
        <h2 className="text-white">
          CONTACTS
        </h2>
      </div>
      <ContactList />
    </div>
  );
};

export default App;
