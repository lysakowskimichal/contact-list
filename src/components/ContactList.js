import React, { Component } from "react";
import Contact from "./Contact";
import "./ContactList.css";
import LoadingInfo from "./LoadingInfo";

class ContactList extends Component {
  state = {
    users: [],
    isLoaded: false,
    alphabetical: "alphabet",
    searchByName: "",
    message: [],
  };

  callbackFunction = (childData) => {
    let tempArr = [...this.state.message];
    if (tempArr.includes(childData)) {
      tempArr = tempArr.filter((el) => {
        return el !== childData;
      });
    } else {
      tempArr = [...this.state.message, childData];
    }
    this.setState({
      message: tempArr,
    });
  };

  async componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url =
      "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";
    const response = await fetch(proxyurl + url);
    const data = await response.json();
    this.setState({
      users: data,
      isLoaded: true,
    });
  }

  handleChange = (e) => {
    this.setState({
      searchByName: e.target.value,
    });
  };

  render() {
    let sortedUsers;
    const { alphabetical, users, searchByName, isLoaded, message } = this.state;

    if (alphabetical === "alphabet") {
      sortedUsers = users.sort((a, b) => (a.last_name > b.last_name ? 1 : -1));
    } else {
      sortedUsers = users.sort((a, b) => (a.last_name < b.last_name ? 1 : -1));
    }

    let filteredUsers = sortedUsers;

    if (searchByName)
      filteredUsers = users.filter(
        (user) =>
          user.last_name.includes(searchByName) ||
          user.first_name.includes(searchByName)
      );

    const u = filteredUsers.map((user) => (
      <Contact
        key={user.id}
        user={user}
        parentCallback={this.callbackFunction}
      />
    ));
    return (
      <div>
        {console.log(message)}
        <input
          onChange={this.handleChange}
          name="searchByName"
          value={searchByName}
          className="form-control"
          type="text"
          placeholder="Choose"
        />
        <ul className="list-group">{isLoaded ? u : <LoadingInfo />}</ul>
      </div>
    );
  }
}

export default ContactList;
