import React from "react";

const LoadingImage = () => {
  return (
    <img
      className="alternateAvatar userAvatar rounded-circle"
      src="https://www.placecage.com/c/150/150
      "
      alt="A"
    />
  );
};

export default LoadingImage;
