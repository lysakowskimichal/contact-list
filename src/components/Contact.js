import React, { Component } from "react";
import "./Contact.css";
import LoadingImage from "./LoadingImage";

class Contact extends Component {
  state = {
    messageId: "",
    altMessageId: "",
    isConfirmed: false,
  };

  sendData = () => {
    this.props.parentCallback(this.state.messageId);
  };

  handleCheckboxChange = () => {
    this.setState({
      isConfirmed: !this.state.isConfirmed,
      messageId: this.props.user.id,
    });
  };

  render() {
    const { user } = this.props;
    const { isConfirmed } = this.state;
    return (
      <li className="list-group-item bg-light">
        <label className="d-flex justify-content-between" htmlFor={user.id}>
          <div className="d-flex justify-content-center align-items-center userAvatarDiv">
            <div className="rounded-circle border userAvatarCnt">
              {user.avatar ? (
                <img
                  src={user.avatar}
                  className="userAvatar rounded-circle"
                  alt="A"
                />
              ) : (
                <LoadingImage />
              )}
            </div>
          </div>
          <div className="text-left flex-grow-1 ">
            <h3 className="userName">
              {user.first_name} {user.last_name}
            </h3>
            <a href={user.email} className="text-muted">
              {user.email}
            </a>
          </div>
          <div className="d-flex justify-content-center align-items-center checkboxContainer">
            <input
              type="checkbox"
              className="userCheckbox"
              id={user.id}
              onChange={this.handleCheckboxChange}
              onInput={this.sendData}
              checked={isConfirmed}
            />
          </div>
        </label>
      </li>
    );
  }
}

export default Contact;
